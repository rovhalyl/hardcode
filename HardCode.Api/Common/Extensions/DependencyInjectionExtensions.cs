﻿using HardCode.Core.Common.Extensions;
using HardCode.Infrastructure.Common.Extensions;

namespace HardCode.Api.Common.Extensions;

internal static class DependencyInjectionExtensions {
    internal static void InjectDependencies(this IServiceCollection services, IConfiguration configuration) {
        services.InjectInfrastructure(configuration);
        services.InjectCore();
    }
}