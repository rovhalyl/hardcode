﻿using HardCode.Core.Common.Interfaces.EntityServices;
using HardCode.Core.Common.Models.EntityModels;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Api.Controllers;

[ApiController]
[Route("api/categories/")]
public class CategoryController: Controller
{
    private readonly ICategoryService _categoryService;

    public CategoryController(ICategoryService categoryService)
    {
        _categoryService = categoryService;
    }

    #region Get
    
    /// <summary>
    /// Метод для получения списка категорий
    /// </summary>
    /// <returns></returns>
    public IActionResult GetCategoriesAsync() {

        return Ok(_categoryService.Entities().Include(x=>x.CategoryAttributes).AsNoTracking());
    }

    #endregion

    #region Post

    /// <summary>
    /// Метод для создания категории
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("create")]
    public async Task<IActionResult> CreateCategoryAsync([FromBody] CategoryModel model)
    {
        return Ok(await _categoryService.CreateCategoryAsync(model));
    }

    #endregion

    #region Put

    /// <summary>
    /// Изменение категории
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut("update")]
    public async Task<IActionResult> UpdateCategoryAsync(CategoryModel model)
    {
        return Ok(await _categoryService.UpdateCategoryAsync(model));
    }

    #endregion
    
    #region Delete

    /// <summary>
    /// Удаление категории
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteCategoryAsync(Guid id)
    {
        await _categoryService.DeleteCategoryAsync(id);
        return Ok();
    }

    #endregion
}