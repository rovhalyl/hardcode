﻿using HardCode.Core.Common.Interfaces.EntityServices;
using HardCode.Core.Common.Models.EntityModels;
using HardCode.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Api.Controllers;

[ApiController]
[Route("api/products/")]
public class ProductController: Controller
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }

    #region Get
    
    /// <summary>
    /// Метод для получения информации о продукте
    /// </summary>
    /// <param name="id">Если вы это прочли, значит докстринги были не зря)</param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpGet("{id:guid}")]
    public async Task<IActionResult> GetProductAsync(Guid id) {
        return Ok(await _productService.Entities().AsNoTracking().FirstOrDefaultAsync(x => x.Id.Equals(id))
                  ?? throw new Exception("Product not found"));
    }
    
    /// <summary>
    /// Получение списка продуктов
    /// </summary>
    /// <param name="categoryId">Айди категории</param>
    /// <param name="additionalFields">Дополнительные поля</param>
    /// <returns></returns>
    [HttpGet("")]
    public  ActionResult<IEnumerable<Product>> GetProductsAsync(Guid categoryId, [FromQuery] Dictionary<string, string>? additionalFields) {

        var filteredProducts = _productService.Entities()
            .Include(x => x.Category)
            .Where(p => p.CategoryId.Equals(categoryId))
            .ToList();

        if (additionalFields != null && additionalFields.Any()) {
            
                filteredProducts =  filteredProducts
                    .Where(p => 
                        p.CategoryFields != null && 
                        additionalFields.All(af => 
                            p.CategoryFields.ContainsKey(af.Key) &&
                            p.CategoryFields[af.Key] == af.Value
                        )
                    )
                    .ToList();
        }

        return filteredProducts.ToList();
    }

    #endregion

    #region Post

    /// <summary>
    /// Метод для создания продукта
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("create")]
    public async Task<IActionResult> CreateProductAsync([FromBody] ProductModel model)
    {
        return Ok(await _productService.CreateProductAsync(model));
    }

    #endregion
    
}