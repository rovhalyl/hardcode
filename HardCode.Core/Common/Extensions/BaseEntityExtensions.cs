﻿using HardCode.Domain.Entities;

namespace HardCode.Core.Common.Extensions;

/// <summary>
/// Класс расширения базовой сущности
/// </summary>
public static class BaseEntityExtensions {
    /// <summary>
    /// Применить сортировку по ID
    /// </summary>
    public static IQueryable<TEntity> EnsureSortingById<TEntity>(this IQueryable<TEntity> entities)
        where TEntity : BaseEntity {
        if (entities is not IOrderedQueryable<TEntity> qo)
            return entities.OrderBy(x => x.Id);
        try {
            return qo.ThenBy(x => x.Id);
        }
        catch {
            return qo.OrderBy(x => x.Id);
        }
    }

    /// <summary>
    /// Фильтрация по ID
    /// </summary>
    public static IQueryable<TBaseEntity> FilterById<TBaseEntity>(this IQueryable<TBaseEntity> entities, string id)
        where TBaseEntity : BaseEntity => entities.Where(x => x.Id == new Guid(id));

    /// <summary>
    /// Фильтрация по ID
    /// </summary>
    public static IQueryable<T> FilterById<T>(this IQueryable<T> entities, Guid id) 
        where T : BaseEntity => entities.Where(x => x.Id == id);

    /// <summary>
    /// Фильтрация по нескольким ID
    /// </summary>
    public static IQueryable<TEntity> FilterByIds<TEntity>(this IQueryable<TEntity> entities, string[] ids)
        where TEntity : BaseEntity => entities.Where(x => ids.Contains(x.Id.ToString()));
    
    /// <summary>
    /// Фильтрация по нескольким ID
    /// </summary>
    public static IQueryable<TEntity> FilterByIds<TEntity>(this IQueryable<TEntity> entities, List<Guid> ids)
        where TEntity : BaseEntity => entities.Where(x => ids.Contains(x.Id));
}