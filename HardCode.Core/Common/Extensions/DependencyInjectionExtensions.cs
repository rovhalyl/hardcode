﻿using HardCode.Core.Common.Interfaces.EntityServices;
using HardCode.Core.EntityServices;
using Microsoft.Extensions.DependencyInjection;

namespace HardCode.Core.Common.Extensions;

public static class DependencyInjectionExtensions
{
    public static void InjectCore(this IServiceCollection services)
    {
        services.AddScoped<ICategoryService, CategoryService>();
        services.AddScoped<IProductService, ProductService>();
    }
}