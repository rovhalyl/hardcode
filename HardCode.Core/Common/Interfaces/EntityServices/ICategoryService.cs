﻿using HardCode.Core.Common.Models.EntityModels;
using HardCode.Domain.Entities;

namespace HardCode.Core.Common.Interfaces.EntityServices;

public interface ICategoryService: IEntityService<Category>
{   
    /// <summary>
    /// Создание категории
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public Task<Category> CreateCategoryAsync(CategoryModel model);
    
    /// <summary>
    /// Обновление категории
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public Task<Category> UpdateCategoryAsync(CategoryModel model);
    
    /// <summary>
    /// Удаление категории
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Task DeleteCategoryAsync(Guid id);
}