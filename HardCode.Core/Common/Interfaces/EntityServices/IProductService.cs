﻿using HardCode.Core.Common.Models.EntityModels;
using HardCode.Domain.Entities;

namespace HardCode.Core.Common.Interfaces.EntityServices;

public interface IProductService: IEntityService<Product>
{
    /// <summary>
    /// Создание продукта
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public Task<Product> CreateProductAsync(ProductModel model);
}