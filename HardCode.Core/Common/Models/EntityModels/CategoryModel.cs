﻿using HardCode.Domain.Entities;
namespace HardCode.Core.Common.Models.EntityModels;

public sealed record CategoryModel(string Name, Guid? Id, List<CategoryAttribute> Attributes);
