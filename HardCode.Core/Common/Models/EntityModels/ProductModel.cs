﻿using System.Text.Json;
using HardCode.Domain.Entities;

namespace HardCode.Core.Common.Models.EntityModels;

public record ProductModel(string Name, string Description, double Price, Guid CategoryId,Guid? Id,Dictionary<string,string>? CategoryFields = null);