﻿using System.Security.Principal;
using HardCode.Core.Common.Interfaces.EntityServices;
using HardCode.Core.Common.Interfaces.Persistence;
using HardCode.Core.Common.Models.EntityModels;
using HardCode.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Core.EntityServices;

internal sealed class CategoryService: EntityService<Category>, ICategoryService
{
    public CategoryService(IDbContext context): base(context) {}
    public override DbSet<Category> Entities() => Context.Categories;
    
    
    public async Task<Category> CreateCategoryAsync(CategoryModel model)
    {
        var category = await Entities().FirstOrDefaultAsync(x => x.Name.Equals(model.Name));
        if (category is not null) throw new Exception($"Category with name = {model.Name} already exists");
        var newCategory = new Category()
        {
            Name = model.Name
        };
        var categoryAttributes = model.Attributes.Select(attr => new CategoryAttribute {
            Key = attr.Key,
        }).ToList();

        newCategory.CategoryAttributes = categoryAttributes;
        await Entities().AddAsync(newCategory);
        await SaveChangesAsync();
        return newCategory;
    }

    public async Task<Category> UpdateCategoryAsync(CategoryModel model)
    {
        var category = await Entities().FirstOrDefaultAsync(x => x.Id.Equals(model.Id));
        if (category is null) throw new Exception("Category not found");
        
        var categoryNameVerification = await Entities().FirstOrDefaultAsync(x => x.Name.Equals(model.Name));
        if (categoryNameVerification is not null) 
            throw new Exception($"Category with name = {model.Name} already exists");
        

        category.Name = model.Name;

        Entities().Update(category);
        await SaveChangesAsync();
        return category;
    }

    public async Task DeleteCategoryAsync(Guid id)
    {
        var category = await Entities().FirstOrDefaultAsync(x => x.Id.Equals(id))
                       ?? throw new Exception("Category not found");
        Entities().Remove(category);
        await SaveChangesAsync();

    }

}