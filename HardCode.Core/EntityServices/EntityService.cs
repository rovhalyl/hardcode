﻿using HardCode.Core.Common.Extensions;
using HardCode.Core.Common.Interfaces.EntityServices;
using HardCode.Core.Common.Interfaces.Persistence;
using HardCode.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace HardCode.Core.EntityServices;

/// <summary>
/// Сервис для работы с сущонстями
/// </summary>
/// <typeparam name="TEntity">Тип сущности</typeparam>
internal abstract class EntityService<TEntity>: IEntityService<TEntity> where TEntity : BaseEntity {
    
    protected EntityService(IDbContext context) {
        Context = context;
    }
    
    public abstract DbSet<TEntity> Entities();
    
    public IDbContext Context { get; }

    public IDbContextTransaction BeginTransaction() => Context.Database.BeginTransaction();
    
    public TEntity GetOne(Guid id) => Entities().FilterById(id).First();
    
    public TEntity GetOne(string id) => Entities().FilterById(id).First();

    public int SaveChanges(bool acceptAllChangesOnSuccess = true) => Context.SaveChanges(acceptAllChangesOnSuccess);

    public async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess = true)
        => await Context.SaveChangesAsync(acceptAllChangesOnSuccess);
}