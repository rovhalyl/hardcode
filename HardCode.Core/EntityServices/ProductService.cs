﻿using System.Text.Json;
using System.Text.Json.Serialization;
using HardCode.Core.Common.Interfaces.EntityServices;
using HardCode.Core.Common.Interfaces.Persistence;
using HardCode.Core.Common.Models.EntityModels;
using HardCode.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Core.EntityServices;

internal sealed class ProductService: EntityService<Product>, IProductService
{
    public ProductService(IDbContext context) : base(context) {}
    
    public override DbSet<Product> Entities() => Context.Products;

    public async Task<Product> CreateProductAsync(ProductModel model)
    {
        var product = await Entities().FirstOrDefaultAsync(x => x.Name.Equals(model.Name));
        var category = await Context.Categories.Include(x => x.CategoryAttributes).FirstOrDefaultAsync(x => x.Id.Equals(model.CategoryId))
                       ?? throw new Exception("Category not found");
        
        if (product is not null) throw new Exception($"Product with name = {model.Name} already exists");
        
        if (model.CategoryFields != null ) {
            if (model.CategoryFields.Count != category.CategoryAttributes.Count) throw new Exception("Invalid count of attributes");
            foreach (var categoryFieldsKey in model.CategoryFields.Keys) {
                if (category.CategoryAttributes.All(x => x.Key != categoryFieldsKey)) {
                    throw new Exception($"Invalid field '{categoryFieldsKey}' was provided");
                }
            }
        }
        
        var newProduct = new Product()
        {
            Name = model.Name,
            Description = model.Description,
            Price = model.Price,
            CategoryId = model.CategoryId,
            CategoryFields = model.CategoryFields
        };
        await Entities().AddAsync(newProduct!);
        await SaveChangesAsync();
        
        return newProduct!;
    }

}