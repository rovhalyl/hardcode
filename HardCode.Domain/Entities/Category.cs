﻿namespace HardCode.Domain.Entities;

public sealed class Category: BaseEntity
{
    #region Plain
    
    /// <summary>
    /// Название категории
    /// </summary>
    public string Name { get; set; } = string.Empty;

    #endregion

    #region Relations

    public List<CategoryAttribute> CategoryAttributes { get; set; } = new();

    #endregion
}