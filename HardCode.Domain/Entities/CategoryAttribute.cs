﻿namespace HardCode.Domain.Entities;

public class CategoryAttribute: BaseEntity
{
    #region Plain
    
    /// <summary>
    /// Ключ
    /// </summary>
    public string Key { get; set; } = string.Empty;
    
    #endregion

    #region Relations

    public Guid CategoryId { get; set; }
    
    public Category? Category { get; set; }

    #endregion
}