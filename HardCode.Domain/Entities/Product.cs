﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
namespace HardCode.Domain.Entities;

public sealed class Product: BaseEntity
{
    #region Plain
    
    /// <summary>
    /// Название продукта
    /// </summary>
    public string Name { get; set; } = string.Empty;
    
    /// <summary>
    /// Описание продукта
    /// </summary>
    public string Description { get; set; } = string.Empty;
    
    /// <summary>
    /// Цена продукта
    /// </summary>
    public double Price  { get; set; }

    /// <summary>
    ///
    /// </summary>
    [Column(TypeName = "jsonb")]
    public Dictionary<string, string>? CategoryFields {get; set; }
    
    
    #endregion

    #region Relations

    public Guid CategoryId { get; set; }
    
    public Category? Category { get; set; }

    /*public List<ProductAttribute> Attributes { get; set; } = new();*/

    #endregion
}