﻿using HardCode.Core.Common.Interfaces;
using HardCode.Core.Common.Interfaces.Persistence;
using HardCode.Infrastructure.Common.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HardCode.Infrastructure.Common.Extensions;

public static class DependencyInjectionExtensions {
    public static void InjectInfrastructure(this IServiceCollection services, IConfiguration configuration) {
        // Подключение базы
        services.AddDbContext<DBContext>(b => b
            .UseNpgsql(configuration.GetConnectionString("Database"), x =>
                x.MigrationsAssembly(System.Reflection.Assembly.GetAssembly(typeof(DBContext))!.FullName))
            .UseApplicationServiceProvider(services.BuildServiceProvider())
            .UseSnakeCaseNamingConvention()
        );
        // Подключение контекста
        services.AddScoped<IDbContext>(provider => provider.GetRequiredService<DBContext>());
    }
    
}