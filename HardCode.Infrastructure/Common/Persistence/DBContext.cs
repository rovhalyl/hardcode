﻿using HardCode.Core.Common.Interfaces;
using HardCode.Core.Common.Interfaces.Persistence;
using HardCode.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Infrastructure.Common.Persistence;

public sealed class DBContext : Microsoft.EntityFrameworkCore.DbContext, IDbContext
{
    public DBContext(DbContextOptions<DBContext> options) : base(options)
    {}

    public DbSet<Category> Categories => Set<Category>();
    public DbSet<Product> Products => Set<Product>();
    public DbSet<CategoryAttribute> ProductAttributes => Set<CategoryAttribute>();


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>().ToTable("category");
        modelBuilder.Entity<Product>().ToTable("product");
        modelBuilder.Entity<CategoryAttribute>().ToTable("product_attribute");

    }

    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
        AddTimestamps();
        return base.SaveChanges(acceptAllChangesOnSuccess);
    }

    public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
        CancellationToken cancellationToken = new())
    {
        AddTimestamps();
        return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }

    /// <summary>
    /// Метод для инициализиц полей CreatedAt и UpdatedAt
    /// </summary>
    private void AddTimestamps()
    {
        var entities = ChangeTracker.Entries()
            .Where(x => x is { Entity: BaseEntity, State: EntityState.Added or EntityState.Modified });

        foreach (var entity in entities)
        {
            var now = DateTime.UtcNow;
            if (entity.State == EntityState.Added)
                ((BaseEntity)entity.Entity).CreatedAt = now;
            ((BaseEntity)entity.Entity).UpdatedAt = now;
        }
    }
}