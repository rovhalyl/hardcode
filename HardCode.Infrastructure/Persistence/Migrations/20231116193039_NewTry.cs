﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HardCode.Infrastructure.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class NewTry : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_product_attribute_product_product_id",
                table: "product_attribute");

            migrationBuilder.DropColumn(
                name: "value",
                table: "product_attribute");

            migrationBuilder.RenameColumn(
                name: "product_id",
                table: "product_attribute",
                newName: "category_id");

            migrationBuilder.RenameIndex(
                name: "ix_product_attribute_product_id",
                table: "product_attribute",
                newName: "ix_product_attribute_category_id");

            migrationBuilder.AddColumn<Dictionary<string, string>>(
                name: "category_fields",
                table: "product",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "fk_product_attribute_category_category_id",
                table: "product_attribute",
                column: "category_id",
                principalTable: "category",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_product_attribute_category_category_id",
                table: "product_attribute");

            migrationBuilder.DropColumn(
                name: "category_fields",
                table: "product");

            migrationBuilder.RenameColumn(
                name: "category_id",
                table: "product_attribute",
                newName: "product_id");

            migrationBuilder.RenameIndex(
                name: "ix_product_attribute_category_id",
                table: "product_attribute",
                newName: "ix_product_attribute_product_id");

            migrationBuilder.AddColumn<string>(
                name: "value",
                table: "product_attribute",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "fk_product_attribute_product_product_id",
                table: "product_attribute",
                column: "product_id",
                principalTable: "product",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
